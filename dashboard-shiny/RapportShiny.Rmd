---
title: "Projet R: Application Shiny : ShinyDashboard"
author: "Pittion Bordigoni Florence, Chaimaa Rizki"
date: "14/12/2020" 
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```


Pour mettre en oeuvre cette application, on a utilisé des données d'une société de télécommunication sur les taux de désabonnement des clients afin de renvoyer des graphiques et des chiffres avec des informations qui peuvent conduire à des développements de produits et à un plan marketing destiné aux clients susceptibles de rester plus longtemps dans l'entreprise.

On a importé et nettoyé les données dans un fichier R dans un dossier "data". On a crée l'application du tableau de bord à l'aide du paquet **R Shiny app**. On a utilisé la bibliothèque **shinydashboard** pour la mise en page de la barre latérale. La bibliothèque **ggplot2** pour la création des graphiques, tandis que gridExtra pour la mise en page des graphiques côte à côte dans certains cas. La fonction de filtrage de la bibliothèque **dplyr** pour filtrer les données. 


## Source du dataset utilisé dans l'application shiny: 
https://www.kaggle.com/blastchar/telco-customer-churn

## Sources de documentation / tutoriels :
https://rstudio-pubs-static.s3.amazonaws.com/440403_2fe4b00a09dd4b268efd6efb353ccad7.html#int%C3%A9gration_dans_shiny 
https://www.youtube.com/watch?v=sJl0EE_RE4o&list=PLH6mU1kedUy_Of03954Dr8Q3r2kFIB-KS&ab_channel=DataScienceTutorials
https://www.youtube.com/watch?v=sJl0EE_RE4o&list=PLH6mU1kedUy8c44XiTkGSEKRwojVHtsHm&ab_channel=DataScienceTutorials
http://rstudio.github.io/shinydashboard/structure.html#body

## Packages utilisés dans l'application
L'ensemble de données brutes est importé et nettoyé dans un fichier R dans le dossier "data". L'application du tableau de bord est créée à l'aide du paquet **R Shiny app** et utilise la bibliothèque **shinydashboard** pour la mise en page de la barre latérale. La bibliothèque **ggplot2** est utilisée pour la création des graphiques, tandis que gridExtra est utilisée pour la mise en page des graphiques côte à côte dans certains cas. La fonction de filtrage de la bibliothèque **dplyr** est utilisée pour filtrer les données. 


## Source du dataset utilisé dans l'application shiny: 
https://www.kaggle.com/blastchar/telco-customer-churn
Avant de visualiser des données, il faut d'abord passer par le preprocessing afin d'avoir des données netoyyées et bien présentées. c'est pour cela, j'ai chargé les données du fichier TelcoCustomerChurn.csv. J'ai fait une sorte de nettoyage et de structuration après jles ai chargées dans l'application shiny.

## 1. But de l'application

Notre objectif est de créer une application de tableau de bord qui pourrait présenter les données sur les désabonnements des clients de manière à aider les équipes de marketing et de vente à comprendre la répartition de leur clientèle et à comparer les taux de désabonnement des groupes démographiques cibles. Les utilisateurs de notre application doivent être capables de filtrer les clients selon une ou plusieurs des catégories suivantes : le sexe, le statut de la relation, s'ils ont des personnes à charge et s'ils sont des personnes âgées. Notre tableau de bord doit renvoyer par la suite les taux d'attrition de la population cible choisie par l'utilisateur. De cette façon,ils peuvent voir clairement quels groupes démographiques ont un taux d'attrition plus élevé. En outre, il donne des informations sur leurs dépenses et les types de services qu'elles préfèrent. L'application affiche ces informations en comparant côte à côte les clients qui ont changé d'adresse et ceux qui sont restés.

### 1.1 Qu'est-ce que le taux de désabonnement des clients ?
Le taux de désaffection des clients est le taux auquel les clients décident de ne plus faire affaire avec une entreprise. Pour une entreprise de télécommunications, le taux de désabonnement est le taux auquel les abonnés abandonnent leurs services et partent chez un concurrent. Il s'agit d'une mesure importante dans le monde des affaires, en particulier dans le secteur des télécommunications, car il est plus coûteux d'acquérir de nouveaux clients que de conserver les clients actuels, compte tenu des budgets de marketing destinés aux non-clients. Une analyse et un suivi continu du taux de désaffection des clients peuvent aider les entreprises à identifier les points faibles et les points forts de leur stratégie de perte de clientèle. Une analyse des personnes les plus susceptibles de partir peut également contribuer à générer des solutions créatives pour des services et des ensembles de services personnalisés.

## 2. Contexte des données
L'ensemble de données est un ensemble de données simulées composé essentiellement de données catégorielles qui décrivent les caractéristiques des clients, telles que l'état des relations et les types de services. Les données numériques discrètes comprennent la durée d'occupation (nombre de mois passés dans l'entreprise). Les données numériques continues comprennent les informations de facturation pour les frais mensuels et totaux. Pour l'objectif principal de ce projet, les variables indépendantes sont les types de données démographiques ou leurs combinaisons. Les variables dépendantes sont les taux de désabonnement et la durée d'occupation pour obtenir des informations générales sur l'attrition, et le type de compte et les types de données sur les services pour obtenir des informations plus détaillées sur la facturation et les habitudes des clients. 

## 3.Prétraitement des données
### 3.1 Nettoyage des données
Les données sont importées et nettoyées dans un fichier appelé CleanData.R, qui est ensuite récupéré dans le script app.R avant que l'objet UI ne soit défini. 

Dans CleanData.R, les valeurs "Oui" et "Non" pour les types de données démographiques ont été modifiées pour être plus descriptives. Par exemple, les valeurs de la colonne "Senior Citizen" sont passées de "Yes" et "No" à "is a Senior" et "is not a Senior". Cela a permis de concevoir l'assurance-chômage de manière claire et descriptive pour le groupe démographique cible. Cela a également simplifié le codage des valeurs d'entrée et de sortie dans R Shiny. Cela a été fait pour les colonnes "Personnes à charge", "Partenaire" et "Personne âgée".

Les valeurs "Oui" et "Non" pour la colonne "Churn" ont été modifiées en "Churn" et "Stayed". Cela a permis de réduire la confusion pour les personnes qui ne connaissent pas la terminologie et qui ne savent pas lire le code.

Les valeurs des ensembles de données "Pas de service internet" et "Pas de service téléphonique" ont été remplacées par "Non". Ces valeurs apparaissent dans des colonnes concernant des services téléphoniques ou internet spécifiques tels que "Lignes multiples" ou "Sauvegarde en ligne". Elles sont redondantes pour ce projet, et ont donc été modifiées pour indiquer simplement "Non" au service.

Enfin, les colonnes "Sexe", "Partenaire", "Personnes à charge", "Personnes âgées" et "Churn" ont été remplacées par des types de données de caractères à lire par l'assurance-chômage.

### 3.2 Conception de l'interface utilisateur
La conception de l'application fait appel à la bibliothèque shinydashboard. Le tableau de bord comporte une barre latérale avec des menus déroulants permettant de sélectionner des filtres démographiques, que l'on peut basculer pour n'exposer que le panneau principal. Le code du tableau de bord de la barre latérale est inclus dans l'annexe. Le panneau principal se compose de cinq sections : Taille démographique, Dépenses démographiques, Durée d'occupation démographique et Services de divertissement. Chaque section est délimitée par un encadré. 

Le thème des couleurs est cohérent dans l'ensemble du tableau de bord, le bleu représentant tous les clients de la population cible, le vert les clients qui sont restés et l'orange les clients qui sont partis.Ces couleurs sont également utilisées dans les graphiques pour montrer la répartition des clients qui sont restés et partis ensemble. Chaque section présente une comparaison côte à côte de tous les clients cibles, des clients qui sont restés et des clients qui sont partis. 

Le tableau de bord utilise également des menus à onglets pour inclure différents types de graphiques pertinents dans chaque section. 

### 3.3 Fonction du serveur
#### 3.3.1 Filtrage des données 
La **fonction la plus importante** de l'application est de filtrer l'ensemble des données par des caractéristiques démographiques. Les filtres sont introduits dans la section UI de l'application R Shiny sous forme de menus déroulants avec des valeurs de données telles que "Homme" et "Femme", chacune avec une option "Pas de filtre". Le filtrage proprement dit de l'ensemble de données est effectué dans la partie serveur de l'application au moyen de la fonction *observe({})*, et utilise la fonction filter() de la bibliothèque dplyr R. 

#### 3.3.2 Création d'un ensemble de données réactives
L'ensemble de données rv$dataset est une valeur réactive, ce qui signifie qu'il est mis à jour à chaque fois que le serveur fonctionne. L'ensemble de données est importé (appelé "dataset") et nettoyé dans un fichier R, puis dans l'application Shiny, il est défini comme une valeur réactive dans les premières lignes de la partie serveur de l'application Shiny.
Le dataset réactif est filtré par barattage pour les graphiques et les valeurs qui se rapportent à un groupe. La fonction dim() obtient un compte de l'ensemble de données réactives, qui nous donne le nombre et les pourcentages de clients dans la première section. 

#### 3.3.3 Boîtes d'information

![](captures/BoiteInfo.PNG)

Les encadrés donnent des chiffres tels que les moyennes pour les types de données donnés. Le code de l'objet ui de l'info box et du fonctionnement du serveur provient de la bibliothèque shinydashboard. Ces info-boxes sont situées côte à côte dans les boîtes de présentation du code de l'application UI. Les boîtes d'information proprement dites sont créées dans la partie serveur de l'application.

Les boîtes d'information donnent les informations suivantes sur tous les clients, les clients restés et les clients churn selon chaque section :
* Taille démographique --> Nombre de clients, Pourcentage du total des clients
* Dépenses démographiques --> Total des frais mensuels, frais mensuels moyens
* Durée d'occupation démographique --> Nombre moyen de mois dans l'entreprise.

#### 3.3.4 Graphiques
Les graphiques sont tous créés à l'aide de la fonction ggplot() de la bibliothèque ggplot2. L'objectif principal de l'application de tableau de bord est de comparer les distributions des différents types de clients. Par conséquent, l'application utilise divers graphiques à barres pour communiquer ces informations. Pour rester cohérent avec le thème des couleurs, les graphiques à barres tels que le graphique de facturation présenté ci-dessus sont créés à l'aide de la fonction grid.arrange() de la bibliothèque GridExtra. De cette façon, la distribution est indiquée pour les différents ensembles de données réactives et montrée ensemble dans un cadre.

## 4. Exploration des données
Voici quelques aperçus que l'application présente sur l'ensemble des données d'IBM Telecom, avec des chiffres notables auxquels on a attribué une étoile ou une double étoile pour le plus important :

### 4.1 Taux de résiliation
![](captures/TauxResil.PNG)
* Le taux de résiliation global est de 26,54%.
* Le sexe ne fait pas de différence dans le taux de désabonnement, les femmes quittant le service 26,92 % du temps, et les hommes 26,16 %.
* Les personnes âgées ont un taux d'attrition beaucoup plus élevé (41,68%) que les personnes non âgées (23,61%). *
* Le statut de la relation fait également une différence dans le taux de désabonnement. Les clients ayant un partenaire ont un taux de résiliation de 19,66 %, tandis que les clients célibataires ont un taux de résiliation de 32,96 %. **
* Les clients ayant des personnes à charge ont également un taux de résiliation beaucoup plus faible (15,45 %) que les clients sans personnes à charge (31,28 %). **
* Les clients célibataires sans personnes à charge (peut-être des diplômés récents de l'université), ont un taux d'attrition de 34,24%. **

### 4.2 Taille démographique
![](captures/TotClient.PNG)
* Le nombre total de clients est de 7 043 (100%). 
* Les sexes sont égaux, les femmes représentant 49,52 % et les hommes 50,48 %.
* Les personnes âgées représentent 16,21 % de la société avec 1 142 clients âgés. De ce groupe, 476 clients churn qui représentent 6,76% de la clientèle totale.
* Les clients ayant un partenaire représentent 48,3% de la clientèle totale. Dans ce groupe, les clients churn représentent 9,5% de la clientèle totale.
* Les clients célibataires représentent 51,70 % de la clientèle totale. Dans ce groupe, les clients churn représentent 17,04 % de la clientèle totale. **
* Les clients ayant des personnes à charge représentent 29,96% de la clientèle totale. Dans ce groupe, les clients churn représentent 4,63% de la clientèle totale. 
* Les clients célibataires sans personnes à charge représentent 46,57% de la clientèle totale de l'entreprise. Dans ce groupe, les clients churn représentent 15,94% de la clientèle totale. **

### 4.3 Type de contrat
![](captures/TypeContrat.PNG)
* La majorité des clients ont un contrat au mois. Les contrats au mois représentent 55,02% de l'ensemble des contrats. Viennent ensuite le contrat de deux ans, avec 24,07 %, et le contrat d'un an, avec 20,91 %. *
* Parmi l'ensemble des clients churn, 88,55% avaient des contrats au mois. 8,88% avaient des contrats d'un an et 2,57% des contrats de deux ans. Il est important de noter que les données ont été recueillies en fonction des taux de désabonnement sur un mois, il faut donc prendre en considération le fait que les clients ayant des contrats plus longs et qui souhaitent partir n'en ont peut-être pas eu la possibilité. 
* Les sexes sont à nouveau proportionnés dans leur préférence de contrat.
* Les personnes âgées signent des contrats mensuels à un taux plus élevé de 70,67 %, les personnes non âgées choisissant des contrats mensuels dans 51,99 % des cas. *
* Les clients ayant un partenaire préfèrent les contrats mensuels, mais signent également des contrats de deux ans à un taux élevé de 35,04 % du temps. Parmi les clients de ce groupe, le contrat de deux ans est le plus populaire, avec 42,48 %. **
* En revanche, les clients isolés préfèrent les contrats plus courts, seuls 13,81 % d'entre eux optant pour un contrat de deux ans, 17,66 % pour un an et 68,53 % pour des contrats mensuels. **
* Les clients ayant des personnes à charge préfèrent les contrats longs, et leur contrat préféré est le contrat de deux ans, à 37,44 %. Ce qui n'écarte que légèrement le contrat mensuel, choisi par 37,39 % des clients de ce groupe. Les contrats annuels représentent 25,17 %.  Parmi les clients qui ne changent pas de fournisseur, les contrats de deux ans sont les plus nombreux, avec 43,17 %. **
* Les clients célibataires sans personnes à charge préfèrent les contrats mensuels à un taux de 69,78%.**

### 4.4 Modes de paiement
![](captures/ModePay.PNG)
* La plupart des clients paient par chèque électronique (33,58 %). Les autres modes de paiement sont proportionnels les uns aux autres : chèque postal à 22,89 %, carte de crédit (automatique) à 21.61 % et virement bancaire (automatique) à 21,92 %. Tous les modes de paiement deviennent très proportionnels parmi les clients qui ne changent pas d'adresse (à moins de 1 % les uns des autres). Parmi les clients churn, le chèque électronique est clairement gagnant, 59,43 % des clients churn payant de cette manière. Les modes de paiement automatiques pour les clients qui ne sont pas des churners sont environ deux fois plus nombreux que ceux utilisés par les clients qui sont des churners. **
* Le genre, ici encore, ne fait aucune différence. 
* Les personnes âgées paient par chèque électronique à un taux plus élevé (52,9 %). En revanche, les personnes non âgées ont une préférence légèrement plus marquée pour les modes de paiement que l'ensemble des personnes âgées. *
* Les clients ayant des partenaires préfèrent les contrôles électroniques, mais parmi ceux qui sont restés automatiques, les méthodes et le contrôle électronique sont étroitement liés. Le chèque postal est clairement perdant pour ce groupe. **
* Les clients célibataires préfèrent toujours le paiement par chèque électronique ; cependant, contrairement aux clients ayant des partenaires, le chèque envoyé par la poste vient en deuxième position. Parmi les clients célibataires qui ne se retournent pas, le chèque postal est le plus populaire. *
* Les clients ayant des personnes à charge préfèrent le chèque électronique, qui vient en dernier lieu et pour les clients churn. Cependant, le chèque électronique est le plus populaire pour les clients qui sont partis. Le chèque envoyé par la poste est légèrement plus performant que les méthodes automatiques. En revanche, les clients sans personnes à charge préfèrent le chèque électronique, qui arrive en tête dans l'ensemble et dans les catégories "churn" et "non-churn". **

### 4.5 Facturation sans papier
![](captures/Facturation.PNG)
* La plupart des clients préfèrent la facturation sans papier (59,22 %), les clients qui changent de fournisseur (74,91 %) la choisissant à un taux plus élevé que les autres (53,56 %).
* Il est surprenant de constater que les personnes âgées choisissent la facturation électronique à un taux plus élevé que le taux global de 76,71 %. Les personnes non âgées sont globalement plus nombreuses à opter pour la dématérialisation (55,84 %), les clients qui ne sont pas des clients réguliers choisissant pour la dématérialisation (près de la moitié) et les clients réguliers pour la dématérialisation (71,93 %).
* Le statut de la relation n'est pas très différent de la préférence générale pour la facturation sans papier.
* Les clients ayant des personnes à charge préfèrent globalement la dématérialisation mais ont une proportion plus élevée de factures papier que les autres groupes démographiques, avec 49,15 % de clients qui choisissent la dématérialisation. Parmi les clients qui n'ont pas changé d'avis, la facturation papier est en fait légèrement plus élevée ici, à 52,07 %. Les clients sans personnes à charge ne préfèrent la dématérialisation que dans une proportion légèrement supérieure à celle de l'ensemble de la clientèle. **
* Les clients célibataires *sans* personnes à charge préfèrent le dématérialisation, à peu près au même rythme que l'ensemble de la clientèle. Les clients célibataires *avec* des personnes à charge préfèrent à 50/50 la dématérialisation, et la facturation papier est plus élevée, à 54,23%, parmi les clients de ce groupe démographique qui ne sont pas retournés chez eux.

### 4.6 Frais
![](captures/Charges.PNG)
* Les frais mensuels moyens s'élèvent globalement à 64,76 . Le total est de 456 116,60 . 
* Les frais mensuels moyens pour les clients perdus (74,44 ) sont plus élevés que pour les clients fidèles (61,27 ).*
* Les personnes âgées ont des frais mensuels beaucoup plus élevés, soit 79,82  au total (79,18  pour les clients qui ne changent pas d'adresse et 80,71  pour les clients qui changent d'adresse). En revanche, les personnes non âgées ont des frais mensuels moyens de 61,85  (58,62  pour les clients qui ne changent pas d'établissement et 72,30 $ pour les clients qui changent d'établissement). **
* Les clients ayant un partenaire ont des frais légèrement plus élevés, tandis que les clients célibataires n'ont que des frais légèrement plus bas. *
* Les clients ayant des personnes à charge ont les frais mensuels les plus bas, en moyenne 59,52  (non-retour à 57,08  et retour à 72,87 $).
* Les clients célibataires avec personnes à charge ont des frais mensuels encore plus bas, en moyenne 52,51  (49,01  pour le non-abonnement et 65,40 $ pour le désabonnement). 

### 4.7 Durée d'occupation
![](captures/Occup.PNG)
* La plupart des clients partent au cours des premiers mois, avec un taux de désabonnement qui diminue, pour ensuite s'égarer vers 30 mois. 
* Le nombre moyen de mois passés dans l'entreprise est de 32,37 mois, avec un taux de non-churn de 37,57 mois et un taux de churn après 17,98 mois*.
* Bien que le taux de désabonnement des seniors soit élevé compte tenu du mois au cours duquel les données ont été recueillies, ce sont des clients fidèles qui désabonnent en moyenne après 21 mois, et ceux qui sont restés en moyenne 42 mois dans l'entreprise*.
* Les clients avec partenaires sont des clients fidèles qui restent en moyenne 42 mois dans l'entreprise (45 mois sans changement d'adresse et 26,59 mois avec changement d'adresse). En revanche, les clients isolés ne restent en moyenne que 23,36 mois (13 mois pour les clients churn).
* Les clients ayant des personnes à charge sont également légèrement plus fidèles que ceux qui n'en ont pas.

### 4.8 Types de services Internet
![](captures/BoiteInfo.PNG)
* La fibre optique est la plus populaire dans l'ensemble. L'ADSL est plus populaire que la fibre optique chez les clients qui ne sont pas en rupture de stock. La fibre optique est très clairement gagnante pour les clients qui changent de fournisseur. 
* Les personnes âgées choisissent la fibre optique à un taux beaucoup plus élevé dans l'ensemble. Les personnes non âgées choisissent l'ADSL.
* Le statut de la relation ne diffère guère de la préférence pour l'Internet dans l'ensemble. 
* Le DSL est clairement gagnant pour les clients ayant des personnes à charge. Les clients sans personnes à charge optent plus souvent pour la fibre optique. 
* 21 % des clients ne bénéficient pas d'un service Internet. 

### 4.9 Types de services téléphoniques
![](captures/ServInternet.PNG)
* Plus de clients ont le service téléphonique qu'Internet. Seuls 9,68% se désistent. Une ligne est la plus populaire, suivie de près par les lignes multiples. Le nombre de lignes multiples est plus élevé pour les clients qui ont choisi une ligne unique et plusieurs lignes au même taux (45 %). 
* Les personnes âgées choisissent les lignes multiples à un taux beaucoup plus élevé que la moyenne. Les lignes multiples sont clairement gagnantes pour ce groupe démographique. **
* Les clients ayant des partenaires choisissent également des lignes multiples à un tarif plus élevé. 
* Les clients ayant des personnes à charge choisissent des contrats de téléphone à une ligne à un tarif légèrement plus élevé. 

### 4.10 Types de services de streaming
![](captures/Stream.PNG)
* Seulement environ 50% des clients se sont inscrits aux services de streaming. Le taux de désabonnement des clients est légèrement supérieur dans l'ensemble. 
* Les personnes âgées diffusent la télévision et les films en continu à un taux très élevé, avec seulement 35 % environ qui se retirent des services de streaming. **
* Les clients ayant des partenaires choisissent les services de streaming à un taux plus élevé que la moyenne, tandis que les clients individuels se désistent.
* Les clients ayant des personnes à charge choisissent également des services de streaming à un taux légèrement plus élevé.

## 5. Résultats et conclusions
### 5.1 L'ensemble de données
Les conclusions basées sur l'ensemble des données et présentées par l'application sont vastes et offrent de nombreuses possibilités de réduire le taux de désabonnement des clients. Les observations mentionnées ci-dessus dans la section 4 peuvent signifier n'importe lequel des éléments suivants ou plus :

* Les clients féminins et masculins n'ont pratiquement aucune différence dans leurs préférences, le marketing devrait donc être neutre sur le plan du genre 
* Les contrats au mois sont très populaires, mais ils donnent plus de possibilités aux clients de partir. Il serait peut-être utile d'envisager une option de contrat entre un mois et un an (un contrat de 6 mois, par exemple). 
* Les clients âgés souscrivent à de nombreux services et paient beaucoup plus en moyenne. Étant donné la nouveauté de certains de ces services, ils ne comprennent peut-être pas tout à fait pourquoi ils paient autant. Les équipes de vente devraient s'en lasser, car les seniors ont également un taux de désabonnement très élevé. 
* Les clients célibataires représentent environ 50% de la clientèle mais ont un taux d'attrition beaucoup plus élevé que les clients ayant un partenaire. Les clients isolés peuvent être frustrés par les coûts, étant donné qu'ils acceptent plus de paiements seuls et utilisent moins les services. La société de télécommunications pourrait envisager un forfait pour les clients célibataires, comme les jeunes diplômés de l'université.
* Les clients semblent plus heureux avec l'ADSL, surtout ceux qui ont un partenaire ou des enfants. Cela indique que le DSL fonctionne mieux dans les grands foyers. Les professionnels de la vente devraient demander combien de personnes utiliseront l'internet pour aider les clients à choisir un forfait. 
* Les incitations à rester dans l'entreprise pendant au moins trois mois au moment de l'inscription peuvent valoir la peine d'être essayées, car le taux de désabonnement diminue considérablement après les deux premiers mois. 
 
### 5.2 L'application
![](captures/App.PNG)
L'application fournit un aperçu approfondi de l'entreprise compte tenu des informations relativement élémentaires contenues dans l'ensemble de données. L'application aide les utilisateurs à explorer rapidement les données sans avoir à utiliser de code. De haut en bas, l'application permet à l'utilisateur d'explorer les informations suivantes et d'en savoir plus sur ses clients : 

Taille démographique des clients : 		
* Combien de clients font partie de la catégorie démographique sélectionnée ?	
* Quel est le pourcentage du total des clients de l'entreprise ?
* Quel est le taux d'attrition de la population sélectionnée ?
* Combien de clients de la population sélectionnée sont partis et combien sont restés ?
* Quel est le pourcentage du total des clients de l'entreprise qui sont partis ou sont restés ?

Dépenses démographiques :
* Quel est le pourcentage de clients de la population sélectionnée qui ont une facturation sans papier ?
* Quelles sont leurs méthodes de paiement ?
* Quels sont leurs types de contrats ?
* Quelles sont les différences de facturation entre les clients qui sont partis et ceux qui sont restés ?
* Quel est le montant de leurs frais mensuels combinés ?
* Combien d'argent l'entreprise a-t-elle perdu en fonction des frais mensuels combinés des clients qui sont partis de la population choisie ?
* Combien ont-ils obtenu des clients qui sont restés ?
* La population choisie dépense-t-elle plus ou moins que les autres ?
* Les clients restants dépensent-ils plus d'argent en moyenne ? 

Durée du séjour :
* Quand les clients quittent-ils l'entreprise ?
* Dans quelle mesure les clients sont-ils fidèles ?
* Combien de temps restent-ils en moyenne ?

Types de service : 
* Quels sont les types de services préférés par la population cible ?
* Quels sont les types de services Internet dont les clients sont satisfaits ?
* Combien de clients ont ce service particulier ?
* Les clients sont-ils satisfaits de ces services ?

### 5.3 Conclusions générales:
L'ensemble de données sur le Churn des clients des télécommunications est relativement simple. Toute petite ou moyenne entreprise peut suivre ses données de cette manière. Cependant, même avec un simple ensemble de données, on peut trouver des informations très importantes pour guider les décisions de l'entreprise. De plus, les ensembles de données simples sont faciles à manipuler avec moins d'employés. Enfin, un tableau de bord facile à utiliser pour l'analyse des clients peut être très précieux, car toutes les informations et conclusions décrites dans les sections 4 et 5 sont claires et faciles à trouver. 






