---
title: "Projet R: Lexique"
author: "Pittion Bordigoni Florence, Chaimaa Rizki"
date: "14/12/2020"
output:
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Utilisation et manipulation de _Lexique 3_

Etant débutantes en R (et en programmation pour l'une d'entre nous) l'idée de cette partie est d'utiliser une base de mots, un exercice un peu éloigné des statistiques mais qui permet de s'approprier les outils de transformation et de visualisation dans R.

Nous allons d'abord présenter l'outil _Lexique 3_ et expliquer dans quelle mesure on va chercher à apporter des propositions alternatives pour l'utiliser.


## Présentation de _Lexique 3_

Lexique est une base de données qui fournit, pour 140 000 mots de la langue française, diverses informations. Par exemple, elle va donner les fréquences d’occurrences dans différents corpus (livres et films), la représentation phonologique, les lemmes associés, le nombre de syllabes, la catégorie grammaticale, et bien d’autres informations...

Cette base permet de faire beaucoup de choses et les auteurs en ont notamment tiré une app shiny très complète (http://www.lexique.org/shiny/openlexicon/).

L'idée n'est donc pas de rivaliser avec ce travail très abouti mais d'utiliser à notre échelle la base pour faire des transformations "alternatives" dans la séléction de mots , leur classification, voir la simplification de l'utilisation en réduisant le nombre de colonnes.

Nous avons décidé d'utiliser les lemmes (variable "lemme") au lieu de la variable "ortho" qui représente tous les mots du lexique, pour éviter trop de redondance des mots (les lemmes étant la forme canonique des mots).


## Parti pris de cette première partie de projet.

### Création d'un objet R

Nous avons choisi de travailler sur une table avec moins de variables et de créer un objet "lexique" avec plusieurs méthodes. Tout cela sera inclu dans un package.

La création de l'objet _lexique_ est vraiment l'atout de ce travail, il va contenir:

* des instructions de chargement de la base.
* une dimension de préparation des données : transformation de certaines variables et séléction de colonnes utilisées dans le projet.
* des méthodes,  à savoir :
  + `countByLetter` : nombre de lemmes groupés par la première lettre.
  + `filterWordFrequency` : fréquence d'apparition des lemmes dans la base de livres.
  + `highestRankCV` : cherche les mots avec le plus de voyelles ou de consonnes par nombre de lettre et optionellement par première lettre des mots.


### Visualisation adaptée

#### Des graphiques

Pour commencer simplement nous avons fait un histogramme de la répartition du nombre de lettre avec dans chaque colonne de l'histogramme la répartition en fonction de la première lettre du mot. 

A travers les données de cette base nous avons choisi de représenter avec des graphiques adaptés à partir des méthode de notre objet "lexique":

- Un premier graphique représe le tri que l'on peut faire avec la méthode `countByLetter` et grace à la colonne p_lettre que nous avons créer dans la préparation du tibble, le graphique choisit est une mosaic du package "treemap", illustrant bien la répartition de cette variable "premières lettre".

- Un second graphique représente un nuage de mots créé avec les packages "wordcloud" et "wordcloud2" que l'on peut faire varier avec les paramètres de la methode `filterWordFrequency` en jouant sur le nombre de lettre ou la première lettre des mots.

#### Un petit jeu

Nous avons aussi créé une méthode `highestRankCV` qui va nous permettre d'obtenir les mot avec le plus de consonnes ou le plus de voyelles et de définir la longeur des mots pour une séléction plus visible, il est aussi possible de filtrer sur la première lettre pour les longueurs de mot comportant beaucoup de résultats.

#### Travail sur l'optimisation de la visualisation

Avec la categorie "cgram" que nous avons simplifié pour passer de 22 à 11 categories, nous avons fait un travail sur la visualisation la plus optimale de la répartition des formes grammaticales dans la base.

Nous avons d'abord découpé progressivement le nombre de lettre en catégotrie jusqu'à arriver à des groupes à peu près homogènes dans les effectifs.

Ensuite nous avons redécoupé la variables "cgram" en deux variables pour plus de lisibilité.

## Conclusion

Encore bien d'autre idées nous sont venues , utilisation des syllabes phonétiques pour les rimes, recherche d' homophones et homographes... mais ce sera pour plus tard...
Dans le même esprit de travail, sur cette base  les possiblités sont vraiments nombreuses.