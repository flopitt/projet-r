# Projet R - M1

> **Pittion Bordigoni Florence, Chaimaa Rizki** 

Ce projet va se décomposer en deux parties. 
Nous avons choisi de développer une première partie autour de l'utilisation de _lexique3_ 
(une base de données des mots de la langue française) avec un package devtools à installer.

La deuxième partie est centrée sur la réalisation d'une app shiny (le rapport de l'app shiny sera sur un second rapport).

# Lexique

## Package
Le package lexique ce situe dans [un dépot Gitlab à part](https://gitlab.com/flopitt/lexique-r) il suffit de suivre les instructions du README pour l'installer.

## Rapport
Dans le dossier [lexique](lexique) se trouvent:
* Le rapport (Rmarkdown+html)
* Le rendu (Rmarkdown+html)

## Packages à installer
* tidyverse
* treemap
* RColorBrewer
* wordcloud2
* wordcloud
* gridExtra
* cowplot

# App Shiny Dashboard

Tous les éléments se trouve dans le dossier [dashboard-shiny](dashboard-shiny)

## Rapport

Le rapport est présent en Rmarkdown+html

## App shiny

Pour lancer l'app shiny, il faut éxécuter le script run-app.R

## Packages à installer
* shiny
* shinydashboard
* gridExtra
* grid
* dplyr
* ggplot2
* plyr